/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include <string>
#include <memory>
#include "Budynek.h"
#include "Czlowiek.h"
#include "IPrintable.h"

Budynek::Budynek()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor budynku"<<std::endl;
	#endif

	nazwa_ = "brak nazwy";
	wlasciciel_ = nullptr;
}

Budynek::Budynek(const Budynek & b)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Budynku"<<std::endl;
	#endif

	nazwa_ = b.nazwa_;
	wlasciciel_ = b.wlasciciel_;
	adres_ = std::move(b.adres_);
}

Budynek::~Budynek()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Destruktor Budynku"<<std::endl;
	#endif
}

void Budynek::print()
{
	InterfaceManager::drawBuildingIcon();
	std::cout << "Nazwa: " << this->nazwa_ << std::endl;
	if (wlasciciel_ != nullptr){
		InterfaceManager::drawPeopleIcon();
		cout << " Wlasciciel: " << *wlasciciel_;
	}
	std::cout << "Adres: ";
	adres_->print();
	std::cout << std::endl;
}

std::ostream & operator<< (std::ostream & os, const Budynek & b)
{
	os << "Nazwa: " << b.nazwa_ << std::endl;
	if(b.wlasciciel_ != nullptr) os << "Wlasciciel: " << *b.wlasciciel_;
	os << "Adres: ";
	os << *b.adres_;
	os << std::endl;

	return os;
}

std::shared_ptr<Adres> Budynek::get_adres()
{
	return adres_;
}

std::string Budynek::get_nazwa()
{
	return nazwa_;
}