/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
#pragma once
#ifndef ADRES_H
#define ADRES_H

#include <iostream>
#include <fstream>
#include "IPrintable.h"
#include "ISaveable.h"

class Adres: public IPrintable<Adres>, public ISaveable
{
	private:
		/**
		 * Ulica
		 * @private
		 */
		std::string ulica_;
		/**
		 * Kod pocztowy
		 * @private
		 */
		std::string kod_pocztowy_;
		/**
		 * Numer budynku [jako std::string, bo mo�e mie� warto�� np 42E]
		 * @private
		 */
		std::string nr_budynku_;
		/**
		 * Numer mieszkania [gdy jest r�wny 0, to znaczy, �e nie ma podzia�u na mieszkania w budynku]
		 * @private
		 */
		unsigned int nr_mieszkania_;


	public:
		/**
		 * Tworzy nowy adres z polami o warto�ci <nieznany> 
		 * @constructor
		 */
		Adres();

		/**
		 * Tworzy nowy adres z podanymi warto�ciami, bez numeru mieszkania
		 * @constructor
		 * @param {std::string} ulica
		 * @param {std::string} kod_pocztowy
		 * @param {std::string} nr_budynku
		 */
		Adres(std::string ulica, std::string kod_pocztowy, std::string nr_budynku);

		/**
		 * Tworzy nowy adres z podanymi warto�ciami
		 * @constructor
		 * @param {std::string} ulica
		 * @param {std::string} kod_pocztowy
		 * @param {std::string} nr_budynku
		 * @param {int}			nr_mieszkania
		 */
		Adres(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania);

		/**
		 * Konstruktor kopiujacy
		 * @constructor
		 */
		explicit Adres(const Adres &);

		/**
		 * Destruktor
		 */
		~Adres();


		/**
		 * Wypisuje informacje o adresie
		 */
		void print();


		//Adres & operator=(const Adres &);

		/**
		 * Wyrzuca adres na strumie�
		 */
		friend std::ostream & operator<< (std::ostream & os, const Adres & t);

		/**
		 * Gettery do p�l prywatnych
		 */
		std::string get_ulica();
		std::string get_kod_pocztowy();
		std::string get_nr_budynku();
		unsigned int get_nr_mieszkania();

		void load(string, map<const string, Czlowiek*>&);
		void save(ofstream&);
};

#endif