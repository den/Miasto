/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include "ZakladPracy.h"

ZakladPracy::ZakladPracy()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor zakladu"<<std::endl;
	#endif

	wlasciciel_ = nullptr;
	nazwa_ = "brak nazwy";
}

ZakladPracy::ZakladPracy(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa, Czlowiek & wlasciciel)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor zakladu"<<std::endl;
	#endif

	nazwa_ = nazwa;
	wlasciciel_ = &wlasciciel;
	adres_ = std::shared_ptr<Adres>(new Adres(ulica, kod_pocztowy, nr_budynku));
}

void ZakladPracy::load(string data, map<const string, Czlowiek*>& people)
{

}

void ZakladPracy::save(ofstream& file)
{
	file << "<budynek type=\"1\" name=\"" + nazwa_ + "\" owner=\"" << wlasciciel_ << "\">";
	adres_->save(file);
	file << "</budynek>";
}