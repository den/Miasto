/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/

#ifndef WIES_H
#define WIES_H

#include "Miasto.h"

class Wies : public Miasto
{
public:
	void print_owner();
};

#endif