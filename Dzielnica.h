/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
#pragma once
#ifndef DZIELNICA_H
#define DZIELNICA_H

#include <iostream>
#include <vector>
#include <fstream>
#include "BudynekMieszkalny.h"
#include "ZakladPracy.h"
#include "IPrintable.h"

using namespace std;

class UnknownBuildingTypeException{};
class BuildingExistsException{};

class Dzielnica : public IPrintable<Dzielnica>, public ISaveable
{
	private:
		/**
		 * Nazwa miasta
		 * @private
		 */
		std::string nazwa_;

		/**
		 * Wektor z budynkami mieszkalnymi
		 * @private
		 */
		std::vector<BudynekMieszkalny> mieszkania_;

		/**
		 * Wektor z zak�adami pracy
		 * @private
		 */
		std::vector<ZakladPracy> zaklady_;

	public:
		/**
		 * Konstruktor bezargumentowy
		 * @constructor
		 */
		Dzielnica();

		/**
		 * Konstruktor z nazw� dzielnicy
		 * @constructor
		 * @param {std::string} nazwa	Nazwa dzielnicy
		 */
		Dzielnica(std::string nazwa);

		/**
		 * Konstruktor kopiuj�cy
		 */
		Dzielnica(const Dzielnica &);

		/**
		 * Destruktor
		 */
		~Dzielnica();

		/**
		 * Dodaje zak�ad pracy do dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {std::string} nazwa			Nazwa zak�adu pracy
		 * @param {Czlowiek &}	wlasciciel		Wska�nik do w�a�ciciela
		 */
		void dodaj_zaklad_pracy(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa, Czlowiek & wlasciciel);

		/**
		 * Dodaje budynek mieszkalny do dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 */
		void dodaj_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku);

		/**
		 * Dodaje budynek mieszkalny do dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {unsigned int}				nr_mieszkania	Numer mieszkania
		 */
		void dodaj_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania);

		/**
		 * Usuwa zak�ad pracy z dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {std::string} nazwa			Nazwa zak�adu pracy
		 */
		void usun_zaklad_pracy(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa);

		/**
		 * Usuwa budynek mieszkalny z dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 */
		void usun_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku);

		/**
		 * Usuwa budynek mieszkalny z dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {unsigned int}				nr_mieszkania	Numer mieszkania
		 */
		void usun_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania);

		/**
		 * Wyrzuca informacje o konkretnych budynkach w dzielnicy na standardowe wyj�cie
		 * @param {int} typ
		 */
		void print();

		/**
		 * Wyrzuca dzielnic� na strumie�
		 */
		friend std::ostream & operator<< (std::ostream & os, const Dzielnica & m);

		/**
		 * Sprawdza czy to te same dzielnice
		 */
		bool operator==(const Dzielnica &);

		/** 
		 * Zwraca nazw� dzielnicy
		 */
		std::string getNazwa();

		void load(string, map<const string, Czlowiek*>&);
		void save(ofstream&);

		/*template<typename T>
		void add(string street, string code, string buildingNo, unsigned int flatNo = 0, string name = "BRAK", Czlowiek * owner = nullptr)
		{
			vector<T>* ptr;

			if (is_same<T, ZakladPracy>::value)
			{
				vector<ZakladPracy> *ptr = &zaklady_;
				//return ptr;
			}
			else if (is_same<T, BudynekMieszkalny>::value)
			{
				vector<BudynekMieszkalny> *ptr = &mieszkania_;
				//return ptr;
			}
			else
				throw UnknownBuildingTypeException();

			/*vector<T>* ptr;
			try
			{
				ptr = resolveBuildingType<T>();
			}
			catch (UnknownBuildingTypeException)
			{
				cerr << "Nie ma takiego typu budynku";
			}

			try
			{
				if (!checkIfExists<T>(ptr, street, code, buildingNo))
				{
					if (is_same<T, ZakladPracy>::value)
						ptr->emplace_back(street, code, buildingNo, name, owner);
					else if (is_same<T, BudynekMieszkalny>::value && flatNo == 0)
						ptr->emplace_back(street, code, buildingNo);
					else
						ptr->emplace_back(street, code, buildingNo, flatNo);
				}
			}
			catch (BuildingExistsException)
			{
				cerr << "Budynek o podanych danych juz istnieje";
			}
		}

		template<typename T>
		bool checkIfExists(vector<T>* ptr, string street, string code, string buildingNo)
		{
			if (ptr->size() > 0)
			{
				for (unsigned int i = 0; i < ptr->size(); i++)
				{
					T obj = ptr->at(i);
					std::shared_ptr<Adres> tmp = obj.get_adres();
					if (tmp->get_ulica() == street && tmp->get_kod_pocztowy() == code && tmp->get_nr_budynku() == buildingNo)
						throw BuildingExistsException();
				}
			}

			return false;
		}

		template<typename T>
		vector<T>* resolveBuildingType()
		{
			//vector<T> *ptr = nullptr;

			/*if (is_same<T, ZakladPracy>::value)
			{
				vector<ZakladPracy> *ptr = &zaklady_;
				return ptr;
			}
			else if (is_same<T, BudynekMieszkalny>::value)
			{
				vector<BudynekMieszkalny> *ptr = &mieszkania_;
				return ptr;
			}
			else
				throw UnknownBuildingTypeException();

			return nullptr;
		}*/
};


#endif