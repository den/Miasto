/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
#pragma once
#ifndef MIASTO_H
#define MIASTO_H

#include <iostream>
#include <memory>
#include <vector>
#include <sstream>
#include <exception>
#include <type_traits>
#include "Dzielnica.h"
#include "BudynekMieszkalny.h"
#include "ZakladPracy.h"

#include "IPrintable.h"
#include "ISaveable.h"
#include "IllegalCityNameException.h"



using namespace std;

class DistrictNotFoundException : exception
{
	string what_;
public:
	DistrictNotFoundException(string district)
	{
		stringstream ss;
		what_ += "Dzielnica nie istnieje. Podana wartosc to: ";
		ss << district;
		what_ += ss.str();
	}
	virtual const char * what() const throw()
	{
		return what_.c_str();
	}
	virtual ~DistrictNotFoundException() throw(){};
};

class DistrictAlreadyExistsException : exception
{
	string what_;
public:
	DistrictAlreadyExistsException(string district)
	{
		stringstream ss;
		what_ += "Dzielnica juz istnieje. Podana wartosc to: ";
		ss << district;
		what_ += ss.str();
	}
	virtual const char * what() const throw()
	{
		return what_.c_str();
	}
	virtual ~DistrictAlreadyExistsException() throw(){};
};

class BuildingNotFoundException : exception
{
	string what_;
public:
	BuildingNotFoundException(string value)
	{
		stringstream ss;
		what_ += "Podany budynek juz istnieje. Wartosc: ";
		ss << value;
		what_ += ss.str();
	}
	virtual const char * what() const throw()
	{
		return what_.c_str();
	}
	virtual ~BuildingNotFoundException() throw(){};
};

class Miasto : public IPrintable<Miasto>, public ISaveable
{
	private:
		/**
		 * Nazwa miasta
		 * @private
		 */
		std::string nazwa_;

		/**
		 * Tablica z dzielnicami
		 * @private
		 */
		std::vector<Dzielnica> dzielnice_;

		/**
		 * Pole statyczne, przedstawia liczb� miast w �yciu programu
		 */
		static unsigned int ileMiast_;

	protected:
		/**
		* Wska�nik na wodza miasta
		* @version 1.2
		*/
		Czlowiek * owner_;

	public:
		/**
		 * Konstruktor bezargumentowy
		 * @constructor
		 */
		Miasto();

		/**
		 * Konstruktor z nazw� miasta
		 * @constructor
		 * @param {std::string} nazwa	Nazwa miasta
		 */
		Miasto(std::string nazwa);

		/**
		 * Konstruktor kopiuj�cy
		 */
		Miasto(const Miasto &);

		/**
		 * Destruktor
		 */
		~Miasto();

		/** 
		 * Dodaje dzielnic� do miasta
		 * @param {std::string} nazwa
		 */
		void dodaj_dzielnice(std::string nazwa) throw();

		/**
		 * Usuwa dzielnic� z miasta
		 * @param {std::string} nazwa
		 */
		void usun_dzielnice(std::string nazwa) throw();

		/**
		 * Dodaje zak�ad pracy do podanej dzielnicy
		 * @param {std::string} dzielnica		Nazwa dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {std::string} nazwa			Nazwa zak�adu pracy
		 * @param {Czlowiek &}	wlasciciel		Wska�nik do w�a�ciciela
		 */
		void dodaj_zaklad_pracy(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa, Czlowiek & wlasciciel) throw();

		/**
		 * Dodaje budynek mieszkalny do podanej dzielnicy
		 * @param {std::string} dzielnica		Nazwa dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 */
		void dodaj_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku) throw();

		/**
		 * Dodaje budynek mieszkalny do podanej dzielnicy
		 * @param {std::string} dzielnica		Nazwa dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {unsigned int}				nr_mieszkania	Numer mieszkania
		 */
		void dodaj_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania) throw();

		/**
		 * Usuwa zak�ad pracy z podanej dzielnicy
		 * @param {std::string} dzielnica		Nazwa dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {std::string} nazwa			Nazwa zak�adu pracy
		 */
		void usun_zaklad_pracy(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa) throw();

		/**
		 * Usuwa budynek mieszkalny z podanej dzielnicy
		 * @param {std::string} dzielnica		Nazwa dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 */
		void usun_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku) throw();

		/**
		 * Usuwa budynek mieszkalny z podanej dzielnicy
		 * @param {std::string} dzielnica		Nazwa dzielnicy
		 * @param {std::string} ulica			Nazwa ulicy
		 * @param {std::string} kod_pocztowy	Kod pocztowy
		 * @param {std::string} nr_budynku		Numer budynku
		 * @param {unsigned int}				nr_mieszkania	Numer mieszkania
		 */
		void usun_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania) throw();

		/**
		 * Wy�wietla na ekranie zawarto�� ca�ego miasta
		 */
		void print();

		/**
		 * Wy�wietla na ekranie zawarto�� danej dzielnicy
		 * @param {std::string} nazwa
		 */
		void pokazStan(std::string nazwa);

		/**
		 * M�wi, ile Miast obecnie istnieje
		 */
		static void pokazIleMiast();

		/**
		 * Wyrzuca miasto na strumie�
		 */
		friend std::ostream & operator<< (std::ostream & os, const Miasto & m);

		/**
		 * Dodaje dzielnic� do miasta, alias dla metody dodaj_dzielnice
		 * @param {std::string} nazwa
		 */
		void operator+=(std::string nazwa);

		/**
		 * Usuwa dzielnic� z miasta, alias dla metody usun_dzielnice
		 * @param {std::string} nazwa
		 */
		void operator!=(std::string nazwa);

		/**
		 * Operator dwuargumentowy, wypisuje zak�ady albo budynki mieszkalne z danej dzielnicy
		 * @param {std::string} dzielnica	Nazwa dzielnicy
		 * @param {int}			typ			0 dla budynk�w mieszkalnych i 1 dla zak�ad�w pracy
		 */
		void operator()(std::string dzielnica) throw();

		/**
		 * Zwraca referencj� do dzielnicy o danym indeksie
		 */
		Dzielnica & operator[](unsigned int i);

		/**
		 * Operator przypisania
		 */
		Miasto & operator=(const Miasto &);

		/**
		 * Sprawdza czy istnieje dzielnica o takiej nazwie
		 */
		bool istniejeDzielnica(std::string nazwa);

		/**
		 * Zmienia nazw� miasta
		 * @version 1.1
		 * @param {string} nazwa Nowa nazwa miasta
		 * @throws IllegalCityNameException
		 */
		void set_nazwa(string nazwa) throw();

		void load(string, map<const string, Czlowiek*>&);
		void save(ofstream&);

		void set_owner(Czlowiek &);

		virtual void print_owner();
};

#endif