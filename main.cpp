/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include <map>
#include <algorithm>

#include "Miasto.h"
#include "Dzielnica.h"
#include "BudynekMieszkalny.h"
#include "ZakladPracy.h"
#include "Adres.h"
#include "Czlowiek.h"
#include "Metropolia.h"
#include "MiastoWojewodzkie.h"
#include "InterfaceManager.h"

#include "XMLParser.h"

using namespace std;

class UnknownCityTypeException : exception{};

// obiekt funkcyjny jest, zaimplementowany, ale jego wykorzystanie nie jest pokazane w pliku main.cpp

// C++0x szaleje, tlyko czemu to nie dzia�a jako statyczna metoda klasy?
template<typename T>
auto requestInput(string msg) -> T
{
	T input;
	cout << msg << "\n";
	cin >> input;

	return input;
}

int main()
{
	bool ifLoaded = false;
	string tmpInput;
	vector<unique_ptr<Miasto>> miasta;
	vector<Czlowiek> ludzie;
	// trzeba, bo realokacja pami�ci wektora i si� mapa  ze wska�nikami psuje
	ludzie.reserve(20);
	map<const string, Czlowiek*> ptrLudzie;

	InterfaceManager::printWelcomeScreen();

	ifstream fi("save");

	if (!fi && InterfaceManager::confirmationDialog("Czy chcesz wczytac przykladowe dane?"))
	{
		miasta.push_back(unique_ptr<Miasto>(new Miasto));
		// Do ka�dej takiej operacji powinien by� blok try..catch je�li program ma wci�� dzia�a� po z�apaniu wyj�tku,
		// tutaj, wczytaj� si� ludzie, chwyci wyj�tek i ca�e miasto si� nie wczyta
		try
		{
			ludzie.emplace_back("Krzysztof", "Rawicz", 55);
			ludzie.emplace_back("Jan", "Kowalski", 38);
			ludzie.emplace_back("Roman", "Nowak", 26);
			// Odkomentowac do testu wyjatkow
			//miasta[0]->dodaj_dzielnice("Wola");
			miasta[0]->dodaj_dzielnice("Wola");
			miasta[0]->set_owner(ludzie[0]);
			miasta[0]->dodaj_dzielnice("Ursynow");
			miasta[0]->dodaj_zaklad_pracy("Wola", "Jakas", "00-123", "30", "Biedronka", ludzie[0]);
			miasta[0]->dodaj_zaklad_pracy("Wola", "Jakas", "00-123", "31", "Biedronka", ludzie[1]);
			miasta[0]->dodaj_budynek_mieszkalny("Wola", "Gorska", "12-345", "32", 8);
		}
		catch (DistrictAlreadyExistsException e) {
			cerr << e.what() << endl;
		}
		catch (DistrictNotFoundException e) {
			cerr << e.what() << endl;
		}
		catch (BuildingNotFoundException e) {
			cerr << e.what() << endl;
		}
	}
	else if (fi && (InterfaceManager::confirmationDialog("Znaleziono plik z zapisem stanu, wczytac?") == true))
	{
		string content;
		string loaded;
		while (getline(fi, content))
			loaded += content;

		vector<string> tmp = XMLParser::split("ludzie", loaded);
		tmp = XMLParser::findIn("czlowiek", "ludzie", tmp[0]);
		for (unsigned int i = 0; i < tmp.size(); i++)
		{
			map<const string, string> atts = XMLParser::attributes(tmp[i]);
			ludzie.emplace_back(atts["imie"], atts["nazwisko"], stoi(atts["wiek"]));
			ptrLudzie.insert(pair<const string, Czlowiek*>(atts["ptr"], (&ludzie[i])));
		}

		string loadedMiasto = XMLParser::getMiasto(loaded);
		map<const string, string> tmp2 = XMLParser::attributes(loadedMiasto);
		switch (stoi(tmp2["type"]))
		{
		case 0:
			miasta.push_back(unique_ptr<Miasto>(new Miasto));
		case 1:
			miasta.push_back(unique_ptr<Miasto>(new Metropolia));
		case 2:
			miasta.push_back(unique_ptr<MiastoWojewodzkie>(new MiastoWojewodzkie));
		default:
			break;
		}
		miasta[0]->load(loadedMiasto, ptrLudzie);
		cout << "Wczytano przykladowe miasto i ludzi" << endl;
		ifLoaded = true;
	}
	// Tak, tu powinien by� wysyp try..catch, �eby bada� czy u�ytkownik nie szaleje
	else //(fi && !ifLoaded && !(InterfaceManager::confirmationDialog("Znaleziono plik z zapisem stanu, wczytac?")))
	{
		int cityType = requestInput<int>("Jakie chcesz miasto stworzyc? 0 - zwykle | 1 - metropolia | 2 - miasto wojewodzkie : ");
		try
		{
			switch (cityType)
			{
			case 0:
				miasta.push_back(unique_ptr<Miasto>(new Miasto));
				break;
			case 1:
				miasta.push_back(unique_ptr<Miasto>(new Metropolia));
				break;
			case 2:
				miasta.push_back(unique_ptr<MiastoWojewodzkie>(new MiastoWojewodzkie));
				break;
			default:
				//cout << "bu";
				throw UnknownCityTypeException();
			}
		}
		catch (UnknownCityTypeException e)
		{
			cerr << "Nieznany typ miasta, konczymy zabawe" << endl;
			exit(1);
		}

		unsigned int peopleAmount = requestInput<unsigned int>("Ilu ludzi chcesz miec pod reka?");
		for (unsigned int i = 0; i < peopleAmount; i++)
		{
			// to wszystko powinno sie sprawdzac wyrazeniami regularnymi
			// ale nie jest
			string imie = requestInput<string>("Podaj imie ludzia: ");
			string nazwisko = requestInput<string>("Podaj nazwisko ludzia: ");
			unsigned int wiek = requestInput<unsigned int>("Podaj wiek ludzia: ");
			ludzie.emplace_back(imie, nazwisko, (int)wiek);
		}

		try {
			miasta[0]->set_nazwa(requestInput<string>("Super! A jak nazwiesz swoje nowe miasto? "));
		}
		catch (IllegalCityNameException e) {
			cerr << e.what() << endl;
		}

		cout << "Teraz pora wybrac wodza tego miasta. ";
		miasta[0]->set_owner(ludzie[InterfaceManager::choosePerson(ludzie)]);

		// nie ma limitu, jak kto� wpisze 100000 to niech sobie je potem zape�nia
		unsigned int districtsAmount = requestInput<unsigned int>("Ile dzielnic chcesz stworzyc? ");
		for (unsigned int i = 0; i < districtsAmount; i++)
		{
			string districtName = requestInput<string>("Nazwij te dzielnice: ");
			miasta[0]->dodaj_dzielnice(districtName);
			unsigned int buildingsAmount = requestInput<unsigned int>("A budynkow to ile chcesz w tej dzielnicy umiescic?");
			for (unsigned int j = 0; j < buildingsAmount; j++)
			{
				if (!(InterfaceManager::confirmationDialog("Jaki to ma byc budynek? 0 - mieszkalny | 1 zaklad pracy", "0", "1")))
				{
					string bulica = requestInput<string>("Dawaj nazwe ulicy");
					string bkod = requestInput<string>("I kod pocztowy");
					string bnrb = requestInput<string>("Uprzejmie prosi sie o podanie nr budynku");
					string bnazwa = requestInput<string>("Odrobina inwencji tworczej, nazwa zakladu");
					cout << "A teraz wlasciciel" << endl;
					int index = InterfaceManager::choosePerson(ludzie);
					miasta[0]->dodaj_zaklad_pracy(districtName,
						bulica,
						bkod,
						bnrb,
						bnazwa,
						ludzie[index]);
				}
				else //mieszkalny
				{
					string bulica = requestInput<string>("Dawaj nazwe ulicy");
					string bkod = requestInput<string>("I kod pocztowy");
					string bnrb = requestInput<string>("Uprzejmie prosi sie o podanie nr budynku");
					unsigned int bnrm = requestInput<unsigned int>("Numerek mieszkanka");

					miasta[0]->dodaj_budynek_mieszkalny(districtName,
						bulica,
						bkod,
						bnrb,
						bnrm);
				}
			}
		}
	}

	miasta[0]->print();
		

	if (InterfaceManager::confirmationDialog("Czy chcesz zapisac to miasto?"))
	{
		ofstream f;
		f.open("save");
		f << "<ludzie>";
		for (unsigned int i = 0; i < ludzie.size(); i++)
			ludzie[i].save(f);
		f << "</ludzie>";

		miasta[0]->save(f);
		f.close();
	}


	


	return 0;
}