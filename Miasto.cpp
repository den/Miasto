/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <regex>
#include <typeinfo>
#include <fstream>

#include "Miasto.h"
#include "BudynekMieszkalny.h"
#include "ZakladPracy.h"
#include "IllegalCityNameException.h"
#include "XMLParser.h"

using namespace std;

unsigned int Miasto::ileMiast_ = 0;

typedef map<const string, string> attributes;

Miasto::Miasto()
{
	//if (dynamic_cast<ISaveable *>(this)) TAG = "miasto";
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Miasta"<<std::endl;
	#endif

	nazwa_ = "Bezimienne miasto";
	this->ileMiast_++;
}

Miasto::Miasto(std::string nazwa)
{
	//if (dynamic_cast<ISaveable *>(this)) TAG = "miasto";
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Miasta"<<std::endl;
	#endif

	nazwa_ = nazwa;
	this->ileMiast_++;
}

Miasto::Miasto(const Miasto & m)
{
	//if (dynamic_cast<ISaveable *>(this)) TAG = "miasto";
	nazwa_ = m.nazwa_;
	dzielnice_ = m.dzielnice_;
	this->ileMiast_++;
}

Miasto::~Miasto()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Destruktor Miasta <" << nazwa_ << ">" <<std::endl;
	#endif

	ileMiast_--;
}


void Miasto::dodaj_dzielnice(std::string nazwa) throw()
{
	if (istniejeDzielnica(nazwa)) dzielnice_.emplace_back(nazwa);
	//else std::cout << "ERROR: Dzielnica " << nazwa << " juz istnieje." << std::endl;
	else
		throw DistrictAlreadyExistsException(nazwa);
}

void Miasto::usun_dzielnice(std::string nazwa) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
		if(!istniejeDzielnica(nazwa))
		{
			dzielnice_.erase(dzielnice_.begin() + i);
			dzielnice_.shrink_to_fit();
			return;
		}

	//std::cout << "ERROR: Dzielnica " << nazwa << " nie istnieje, nie ma czego usuwac." << std::endl;
	throw DistrictNotFoundException(nazwa);
}

void Miasto::dodaj_zaklad_pracy(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa, Czlowiek & wlasciciel) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnica == dzielnice_[i].getNazwa())
		{
			dzielnice_[i].dodaj_zaklad_pracy(ulica, kod_pocztowy, nr_budynku, nazwa, wlasciciel);
			return;
		}
	}
	//std::cout << "ERROR: Dzielnica " << dzielnica << " nie istnieje, nie ma gdzie dodac budynku." << std::endl;
	throw DistrictNotFoundException(dzielnica);
}

void Miasto::dodaj_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnica == dzielnice_[i].getNazwa())
		{
			dzielnice_[i].dodaj_budynek_mieszkalny(ulica, kod_pocztowy, nr_budynku);
			return;
		}
	}
	//std::cout << "ERROR: Dzielnica " << dzielnica << " nie istnieje, nie ma gdzie dodac budynku." << std::endl;
	throw DistrictNotFoundException(dzielnica);
}

void Miasto::dodaj_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnica == dzielnice_[i].getNazwa())
		{
			dzielnice_[i].dodaj_budynek_mieszkalny(ulica, kod_pocztowy, nr_budynku, nr_mieszkania);
			return;
		}
	}
	//std::cout << "ERROR: Dzielnica " << dzielnica << " nie istnieje, nie ma gdzie dodac budynku." << std::endl;
	throw DistrictNotFoundException(dzielnica);
}

void Miasto::usun_zaklad_pracy(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnice_[i].getNazwa() == dzielnica)
		{
			dzielnice_[i].usun_zaklad_pracy(ulica, kod_pocztowy, nr_budynku, nazwa);
			return;
		}
	}
	//std::cout << "ERROR: Nie ma czego usuwac, podany zaklad pracy nie istnieje" << std::endl;
	throw BuildingNotFoundException("zaklad pracy");
}

void Miasto::usun_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnice_[i].getNazwa() == dzielnica)
		{
			dzielnice_[i].usun_budynek_mieszkalny(ulica, kod_pocztowy, nr_budynku);
			return;
		}
	}
	//std::cout << "ERROR: Nie ma czego usuwac, podany budynek mieszkalny nie istnieje" << std::endl;
	throw BuildingNotFoundException("budynek mieszkalny");
}

void Miasto::usun_budynek_mieszkalny(std::string dzielnica, std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnice_[i].getNazwa() == dzielnica)
		{
			dzielnice_[i].usun_budynek_mieszkalny(ulica, kod_pocztowy, nr_budynku, nr_mieszkania);
			return;
		}
	}
	//std::cout << "ERROR: Nie ma czego usuwac, podany budynek mieszkalny nie istnieje" << std::endl;
	throw BuildingNotFoundException("budynek mieszkalny");
}

void Miasto::print()
{
	std::cout << std::endl << "=============== " << nazwa_ << " ===============" << std::endl;
	print_owner();
	if(dzielnice_.size() > 0)
		for_each(dzielnice_.begin(), dzielnice_.end(), [](Dzielnica d){
			d.print();
		});
	else
		std::cout << "<brak dzielnic w tym miescie>" << std::endl;
}

void Miasto::pokazIleMiast()
{
	std::cout << "O tyle jest miast: " << ileMiast_ << std::endl;
}

std::ostream & operator<< (std::ostream & os, const Miasto & m)
{
	os << std::endl << "=============== " << m.nazwa_ << " ===============" << std::endl;
	if(m.dzielnice_.size() > 0)
		for_each(m.dzielnice_.begin(), m.dzielnice_.end(), [](Dzielnica d){
		d.print();
	});
	else
		os << "<brak dzielnic w tym miescie>" << std::endl;

	return os;
}

void Miasto::operator+=(std::string nazwa)
{
	dodaj_dzielnice(nazwa);
}

void Miasto::operator!=(std::string nazwa)
{
	usun_dzielnice(nazwa);
}

void Miasto::operator()(std::string dzielnica) throw()
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(dzielnica == dzielnice_[i].getNazwa())
		{
				dzielnice_[i].print();
				return;
		}
	}
	throw DistrictNotFoundException(dzielnica);
	//std::cout << "ERROR: Dzielnica " << dzielnica << " nie istnieje." << std::endl;
}

Dzielnica & Miasto::operator[](unsigned int i)
{
	if(dzielnice_.size() >= i) return dzielnice_[i];
	else 
	{
		std::cout << "ERROR: Nie ma dzielnicy o danym indeksie" << std::endl;
		return dzielnice_[i];
	}
}

Miasto & Miasto::operator=(const Miasto & m)
{
	if(this != &m)
	{
		nazwa_ = m.nazwa_;
		dzielnice_ = m.dzielnice_;
	}

	return *this;
}

bool Miasto::istniejeDzielnica(std::string nazwa)
{
	for(unsigned int i = 0; i < dzielnice_.size(); i++)
	{
		if(nazwa == dzielnice_[i].getNazwa())
		{
			return false;
		}
	}
	return true;
}

void Miasto::set_nazwa(string nazwa) throw()
{
	if (!regex_match(nazwa, regex("^[a-zA-Z- ]+$")))
		throw IllegalCityNameException("Niedozwolona nazwa miasta. ", nazwa);
}

void Miasto::load(string data, map<const string, Czlowiek*>& people)
{
	map<const string, string> atts = XMLParser::attributes(data);
	nazwa_ = atts["name"];

	//map<const string, Czlowiek*> tmp = people;
	if (!(people.find(atts["owner"]) == people.end()))
		owner_ = people.find(atts["owner"])->second;

	vector<string> districts = XMLParser::split("dzielnica", data);
	for (unsigned int i = 0; i < districts.size(); i++)
	{
		dzielnice_.emplace_back();
		dzielnice_[i].load(districts[i], people);
	}
}

void Miasto::save(ofstream& file)
{
	file << "<miasto name=\"" + nazwa_ + "\" type=\"0\" owner=\"" << owner_ << "\">";
	for (unsigned int i = 0; i < dzielnice_.size(); i++)
		dzielnice_[i].save(file);
	file << "</miasto>";
}

void Miasto::set_owner(Czlowiek& ptr)
{
	owner_ = &ptr;
}

void Miasto::print_owner()
{
	cout << "Burmistrzem tego miasta jest: " << *owner_;
}