/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/
#pragma once
#ifndef METROPOLIA_H
#define METROPOLIA_H

#include "Miasto.h"

class Metropolia : public Miasto
{
public:
	void print_owner();
};

#endif