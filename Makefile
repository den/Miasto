CC=g++
#CFLAGS=-c -Wall -std=c++11 -D TESTPR1
CFLAGS=-c -Wall -std=c++11 
LDFLAGS=
SOURCES=main.cpp IllegalCityNameException.cpp IPrintable.cpp ISaveable.cpp Adres.cpp Budynek.cpp BudynekMieszkalny.cpp Czlowiek.cpp Dzielnica.cpp InterfaceManager.cpp Metropolia.cpp Miasto.cpp MiastoWojewodzkie.cpp XMLParser.cpp ZakladPracy.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=miasto

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
	
clean:
	rm -f *.o $(EXECUTABLE)
	
run:
	./$(EXECUTABLE)
