/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
#pragma once
#ifndef BUDYNEK_H
#define BUDYNEK_H

#include <iostream>
#include <memory>
#include <vector>
#include "Adres.h"
#include "Czlowiek.h"
#include "IPrintable.h"
#include "InterfaceManager.h"

class Budynek : public IPrintable<Budynek>
{
	protected:
		/**
		 * Nazwa budynku
		 */
		std::string nazwa_;

		/**
		 * Wska�nik na Adres
		 */
		//TODO: przerobi� na unique_ptr
		std::shared_ptr<Adres> adres_;

		/**
		 * Wska�nik na w�a�ciciela
		 */
		//TODO: przerobi� na smart_ptr
		Czlowiek * wlasciciel_;

	public:
		/**
		 * Tworzy budynek z pustym adresem i nazw�
		 * @constructor
		 */
		Budynek();

		/**
		 * Konstruktor kopiuj�cy
		 * @constructor
		 */
		Budynek(const Budynek &);

		/**
		 * Destruktor
		 */
		~Budynek();

		/**
		 * Wyrzuca informacje o budynku na standardowe wyj�cie
		 */
		void print();


		/**
		 * Wyrzuca budynek na strumie�
		 */
		friend std::ostream & operator<< (std::ostream & os, const Budynek & b);

		/**
		 * Getter adresu
		 */
		std::shared_ptr<Adres> get_adres();

		/**
		 * Getter nazwy
		 */
		std::string get_nazwa();
};

#endif