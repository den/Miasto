/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/

#include <iostream>
#include <exception>

#ifndef ILLEGAL_CITY_NAME_E_H
#define ILLEGAL_CITY_NAME_E_H

class IllegalCityNameException : public exception
{
	string name_;
	string what_;
public:
	IllegalCityNameException(const char * what, string name) :what_(what), name_(name)
	{
		stringstream ss;
		what_ += "Nazwa miasta ustawiona na: ";
		ss << name_;
		what_ += ss.str();
	}
	virtual const char * what() const throw()
	{
		return what_.c_str();
	}
};

#endif