/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include <fstream>
#include "Czlowiek.h"
#include "IPrintable.h"

Czlowiek::Czlowiek()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor cz�owieka"<<std::endl;
	#endif

	imie_ = "<imie>";
	nazwisko_ = "<nazwisko>";
	wiek_ = 0;
}

Czlowiek::Czlowiek(std::string imie, std::string nazwisko, unsigned int wiek)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor cz�owieka"<<std::endl;
	#endif

	imie_ = imie;
	nazwisko_ = nazwisko;
	wiek_ = wiek;
}

Czlowiek::~Czlowiek()
{
	#ifdef TESTPR1
		std::cout<<"INFO: destruktor czlowieka <" << imie_ << " " << nazwisko_ << ", " << wiek_ << ">" <<std::endl;
	#endif
}

//Czlowiek::Czlowiek(const Czlowiek & c)
//{
//	imie_ = c.imie_;
//	nazwisko_ = c.nazwisko_;
//	wiek_ = c.wiek_;
//}

void Czlowiek::print()
{
	std::cout << imie_ << " " << nazwisko_ << ", " << wiek_ << std::endl;
}

Czlowiek::operator std::string()
{
	return "Czesc, jestem " + imie_ + " " + nazwisko_ + " i mam " + std::to_string(wiek_) + " lat.";
}

std::ostream & operator<<(std::ostream & os, const Czlowiek & c)
{
	os << c.imie_ << " " << c.nazwisko_ << ", " << c.wiek_ << std::endl;
	return os;
}

void Czlowiek::load(string data, map<const string, Czlowiek*>& people)
{

}

void Czlowiek::save(ofstream& file)
{
	Czlowiek * ptr = this;
	file << "<czlowiek ptr=\"" << ptr << "\" imie=\"" + imie_ + "\" nazwisko=\"" + nazwisko_ + "\" wiek=\"" << wiek_ << "\"></czlowiek>";
}