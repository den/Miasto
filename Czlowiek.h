/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
//#ifdef _WIN32
#pragma once
//#endif

#pragma once
#ifndef CZLOWIEK_H
#define CZLOWIEK_H

#include <iostream>
#include <string>
#include "IPrintable.h"
#include "ISaveable.h"

class Czlowiek : public IPrintable<Czlowiek>, public ISaveable
{
	private:
		/**
		 * Imi�
		 * @private
		 */
		std::string imie_;
		/**
		 * Nazwisko
		 * @private
		 */
		std::string nazwisko_;
		/**
		 * Wiek
		 * @private
		 */
		unsigned int wiek_;

	public:
		/**
		 * Tworzy cz�owieka bez zdefiniowanego czegokolwiek, taki podcz�owiek z niego
		 * @constructor
		 */
		Czlowiek();

		/**
		 * Tworzy cz�owieka z podanymi parametrami
		 * @constructor
		 * @param {std::string} imie
		 * @param {std::string} nazwisko
		 * @param {int}			wiek
		 */
		Czlowiek(std::string imie, std::string nazwisko, unsigned int wiek);

		/**
		 * Destruktor cz�owieka, jakkolwiek to brzmi
		 */
		~Czlowiek();

		//Czlowiek(const Czlowiek &);

		/**
		 * Wypisuje dane cz�owieka
		 */
		void print();

		operator std::string();

		/**
		 * Wyrzuca cz�owieka na strumie�
		 */
		friend std::ostream & operator<< (std::ostream & os, const Czlowiek & c);

		void load(string data, map<const string, Czlowiek*>& people);
		void save(ofstream&);
};

#endif