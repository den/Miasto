/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/
#pragma once
#ifndef IPRINTABLE_H
#define IPRINTABLE_H

template<typename T>
class IPrintable
{
public:
	// Wyświetla zawartość obiektu i jego podobiektów
	friend std::ostream & operator<< (std::ostream & os, const T &);

protected:
	// Wyświetla zawartość obiektu i jego podobiektów
	virtual void print() = 0;
};

#endif