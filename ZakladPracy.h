/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
#pragma once
#ifndef ZAKLAD_PRACY_H
#define ZAKLAD_PRACY_H

#include <iostream>
#include <memory>
#include <fstream>
#include "Budynek.h"
#include "Adres.h"
#include "Czlowiek.h"

class ZakladPracy: public Budynek
{
	public:
		explicit ZakladPracy();
		explicit ZakladPracy(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa, Czlowiek & wlasciciel);
		void load(string, map<const string, Czlowiek*>&);
		void save(ofstream&);
};

#endif
