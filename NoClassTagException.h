/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/

#include <iostream>
#include <exception>
#include <sstream>

using namespace std;

#ifndef NO_CLASS_TAG_E_H
#define NO_CLASS_TAG_E_H

class NoClassTagException : public exception
{
	string name_;
	string what_;
public:
	NoClassTagException(const char * what) :what_(what)
	{
		what_ = "Nazwa miasta ustawiona na: ";
	}
	virtual const char * what() const throw()
	{
		return what_.c_str();
	}
};

#endif