/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */
#pragma once
#ifndef MIESZKANIE_H
#define MIESZKANIE_H

#include <iostream>
#include <memory>
#include <fstream>
#include "Budynek.h"
#include "Adres.h"
#include "Czlowiek.h"
#include "ISaveable.h"

class BudynekMieszkalny: public Budynek, public ISaveable
{
	public:
		explicit BudynekMieszkalny();
		explicit BudynekMieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku);
		explicit BudynekMieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, int nr_mieszkania);
		void load(string, map<const string, Czlowiek*>&);
		void save(ofstream&);
};

#endif