/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include <string>
#include "Adres.h"

Adres::Adres()
{
	ulica_ = "<nieznany>";
	kod_pocztowy_ = "<nieznany>";
	nr_budynku_ = "<nieznany>";
	nr_mieszkania_ = 0;
}

Adres::Adres(std::string ulica, std::string kod_pocztowy, std::string nr_budynku)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Adresu"<<std::endl;
	#endif

	ulica_ = ulica;
	kod_pocztowy_ = kod_pocztowy;
	nr_budynku_ = nr_budynku;
	nr_mieszkania_ = 0;
}

Adres::Adres(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Adresu"<<std::endl;
	#endif

	ulica_ = ulica;
	kod_pocztowy_ = kod_pocztowy;
	nr_budynku_ = nr_budynku;
	nr_mieszkania_ = nr_mieszkania;
}

Adres::Adres(const Adres & a)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Adresu"<<std::endl;
	#endif

	ulica_ = a.ulica_;
	kod_pocztowy_ = a.kod_pocztowy_;
	nr_budynku_ = a.kod_pocztowy_;
	nr_mieszkania_ = a.nr_mieszkania_;
}

Adres::~Adres()
{
	#ifdef TESTPR1
	std::cout<<"INFO: Destruktor Adresu <" << ulica_ << " " << nr_budynku_ << "/" << nr_mieszkania_ << ", " << kod_pocztowy_ << ">" <<std::endl;
	#endif
}

void Adres::print()
{
	std::cout << ulica_ << " " << nr_budynku_;
	if(nr_mieszkania_ > 0) std::cout << "/" << nr_mieszkania_;
	std::cout << ", " << kod_pocztowy_;
}

std::ostream & operator<<(std::ostream & os, const Adres & a)
{
	os << a.ulica_ << " " << a.nr_budynku_;
	if(a.nr_mieszkania_ > 0) os << "/" << a.nr_mieszkania_;
	os << ", " << a.kod_pocztowy_;

	return os;
}

std::string Adres::get_ulica()
{
	return ulica_;
}

std::string Adres::get_kod_pocztowy()
{
	return kod_pocztowy_;
}

std::string Adres::get_nr_budynku()
{
	return nr_budynku_;
}

unsigned int Adres::get_nr_mieszkania()
{
	return nr_mieszkania_;
}

void Adres::load(string data, map<const string, Czlowiek*> &people)
{

}

void Adres::save(ofstream& file)
{
	file << "<adres ulica=\"" << ulica_ << "\" kod=\"" + kod_pocztowy_ + "\" nrb=\"" + nr_budynku_ + "\"  nrm=\"" << nr_mieszkania_ << "\"></adres>";
}