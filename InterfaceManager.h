/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/

#ifndef INTERFACE_MANAGER_H
#define INTERFACE_MANAGER_H

#include <string>
#include "Czlowiek.h"

using namespace std;

class InterfaceManager
{
private:
	
public:
	InterfaceManager();
	~InterfaceManager();

	static void printWelcomeScreen();

	//template<typename T>
	//static auto requestInput(string) -> T;

	static int choosePerson(vector<Czlowiek>& people);
	
	static bool confirmationDialog(string msg = "Czy jestes pewien?", string yes = "T", string no = "N");

	static void drawBuildingIcon();

	static void drawPeopleIcon();
};

#endif // !INTERFACE_MANAGER_H
