/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/

#include <iostream>
#include <string>
#include <vector>
#include "InterfaceManager.h"
#include "Czlowiek.h"

using namespace std;

void InterfaceManager::printWelcomeScreen()
{
	cout << "                                                           .-'''-.         ___   " << endl;
	cout << "                                                          '   _    \\    .'/   \\  " << endl;
	cout << " __  __   ___    .--.                                   /   /` '.   \\  / /     \\ " << endl;
	cout << "|  |/  `.'   `.  |__|                                  .   |     \\  '  | |     | " << endl;
	cout << "|   .-.  .-.   ' .--.                             .|   |   '      |  ' | |     | " << endl;
	cout << "|  |  |  |  |  | |  |     __                    .' |_  \\    \\     / /  |/`.   .' " << endl;
	cout << "|  |  |  |  |  | |  |  .:--.'.          _     .'     |  `.   ` ..' /    `.|   |  " << endl;
	cout << "|  |  |  |  |  | |  | / |   \\ |       .' |   '--.  .-'     '-...-'`      ||___|  " << endl;
	cout << "|  |  |  |  |  | |  | `\\\" __ | |      .   | /    |  |                     |\\/___\\/  " << endl;
	cout << "|__|  |__|  |__| |__|  .'.''| |    .'.'| |//    |  |                     .'.--.  " << endl;
	cout << "                      / /   | |_ .'.'.-'  /     |  '.'                  | |    | " << endl;
	cout << "                      \\ \\._,\\ '/ .'   \\_.'      |   /                   \\_\\    / " << endl;
	cout << "                       `--'  `\\\"                 `'-'                     `''--'  " << endl;
	cout << "\n\n";
	cout << "Zbuduj z nami swoje miasto! [PROE Projekt 2]" << endl;
}

/*template<typename T>
static auto InterfaceManager::requestInput(string msg) -> T
{
	T input;
	cout << msg << "\n";
	cin >> input;

	return input;
}*/

int InterfaceManager::choosePerson(vector<Czlowiek>& people)
{
	cout << "Wybierz ludzia:" << endl;
	for (unsigned int i = 0; i < people.size(); i++)
	{
		cout << "[" << i << "] " << people[i] << "\n";
	}
	unsigned int input;
	cout << "Wybor: ";
	cin >> input;
	if (input > people.size())
	{
		choosePerson(people);
	}
	return input;
}

bool InterfaceManager::confirmationDialog(string msg, string yes, string no)
{
	string input;
	cout << msg << " [" << yes << "/" << no <<"]: ";
	cin >> input;
	if (input == yes)
		return true;
	else if (input == no)
		return false;
	else
		confirmationDialog(msg, yes, no);
}

void InterfaceManager::drawBuildingIcon()
{
	cout << "         _______   " << endl;
	cout << "         |     |   " << endl;
	cout << ".........| []  |..." << endl;
}

void InterfaceManager::drawPeopleIcon()
{
	cout << "(^_^)";
}

//template<typename T>
//string InterfaceManager::requestInput(string msg);