/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <type_traits>
#include "Dzielnica.h"
#include "Budynek.h"
#include "BudynekMieszkalny.h"
#include "ZakladPracy.h"
#include "Czlowiek.h"
#include "XMLParser.h"

using namespace std;

Dzielnica::Dzielnica()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Dzielnicy"<<std::endl;
	#endif

	nazwa_ = "<dzielnica bez nazwy>";
}

Dzielnica::Dzielnica(std::string nazwa)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor Dzielnicy"<<std::endl;
	#endif

	nazwa_ = nazwa;
}

Dzielnica::Dzielnica(const Dzielnica & d)
{
	nazwa_ = d.nazwa_;
	mieszkania_ = d.mieszkania_;
	zaklady_ = d.zaklady_;
}

Dzielnica::~Dzielnica()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Destruktor dzielnicy <" << nazwa_ << ">" <<std::endl;
	#endif
}

void Dzielnica::dodaj_zaklad_pracy(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa, Czlowiek & wlasciciel)
{
	if(zaklady_.size() > 0)
	{
		for(unsigned int i = 0; i < zaklady_.size(); i++)
		{
			std::shared_ptr<Adres> tmp = zaklady_[i].get_adres();
			if(tmp->get_ulica() == ulica && tmp->get_kod_pocztowy() == kod_pocztowy && tmp->get_nr_budynku() == nr_budynku)
			{
				std::cout << "ERROR: Taki budynek juz instnieje w podanej dzielnicy" << std::endl;
				return;
			}
		}
	}
	zaklady_.emplace_back(ulica, kod_pocztowy, nr_budynku, nazwa, wlasciciel);
}

void Dzielnica::dodaj_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku)
{
	if(mieszkania_.size() > 0)
	{
		for(unsigned int i = 0; i < mieszkania_.size(); i++)
		{
			std::shared_ptr<Adres> tmp = mieszkania_[i].get_adres();
			if(tmp->get_ulica() == ulica && tmp->get_kod_pocztowy() == kod_pocztowy && tmp->get_nr_budynku() == nr_budynku)
			{
				std::cout << "ERROR: Taki budynek juz instnieje w podanej dzielnicy" << std::endl;
				return;
			}
		}
	}
	mieszkania_.emplace_back(ulica, kod_pocztowy, nr_budynku);
}

void Dzielnica::dodaj_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania)
{
	if(mieszkania_.size() > 0)
	{
		for(unsigned int i = 0; i < mieszkania_.size(); i++)
		{
			std::shared_ptr<Adres> tmp = mieszkania_[i].get_adres();
			if(tmp->get_ulica() == ulica && tmp->get_kod_pocztowy() == kod_pocztowy && tmp->get_nr_budynku() == nr_budynku && tmp->get_nr_mieszkania() == nr_mieszkania)
			{
				std::cout << "ERROR: Taki budynek juz instnieje w podanej dzielnicy" << std::endl;
				return;
			}
		}
	}
	mieszkania_.emplace_back(ulica, kod_pocztowy, nr_budynku, nr_mieszkania);
}

void Dzielnica::usun_zaklad_pracy(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, std::string nazwa)
{
	if(zaklady_.size() > 0)
	{
		for(unsigned int i = 0; i < zaklady_.size(); i++)
		{
			std::shared_ptr<Adres> tmp = zaklady_[i].get_adres();
			if(tmp->get_ulica() == ulica && tmp->get_kod_pocztowy() == kod_pocztowy && tmp->get_nr_budynku() == nr_budynku && zaklady_[i].get_nazwa() == nazwa)
			{
				zaklady_.erase(zaklady_.begin() + i);
				zaklady_.shrink_to_fit();
				return;
			}
		}
	}
	std::cout << "ERROR: Nie ma czego usuwac, w tej dzielnicy nie ma zadnego budynku." << std::endl;
}

void Dzielnica::usun_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku)
{
	if(mieszkania_.size() > 0)
	{
		for(unsigned int i = 0; i < mieszkania_.size(); i++)
		{
			std::shared_ptr<Adres> tmp = mieszkania_[i].get_adres();
			if(tmp->get_ulica() == ulica && tmp->get_kod_pocztowy() == kod_pocztowy && tmp->get_nr_budynku() == nr_budynku)
			{
				zaklady_.erase(zaklady_.begin() + i);
				zaklady_.shrink_to_fit();
				return;
			}
		}
	}
	std::cout << "ERROR: Nie ma czego usuwac, w tej dzielnicy nie ma zadnego budynku." << std::endl;
}

void Dzielnica::usun_budynek_mieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, unsigned int nr_mieszkania)
{
	if(mieszkania_.size() > 0)
	{
		for(unsigned int i = 0; i < mieszkania_.size(); i++)
		{
			std::shared_ptr<Adres> tmp = mieszkania_[i].get_adres();
			if(tmp->get_ulica() == ulica && tmp->get_kod_pocztowy() == kod_pocztowy && tmp->get_nr_budynku() == nr_budynku && tmp->get_nr_mieszkania() == nr_mieszkania)
			{
				zaklady_.erase(zaklady_.begin() + i);
				zaklady_.shrink_to_fit();
				return;
			}
		}
	}
	std::cout << "ERROR: Nie ma czego usuwac, w tej dzielnicy nie ma zadnego budynku." << std::endl;
}

//TODO: wyjatki a nie else
void Dzielnica::print()
{
	std::cout << std::endl << "----- Lista obiektow w dzielnicy " << nazwa_ << ": -----" << std::endl;
		std::cout << "Budynki mieszkalne:" << std::endl;
		if(mieszkania_.size() > 0)
			for(unsigned int i = 0; i < mieszkania_.size(); i++)
			{
				std::cout << i+1 << "." << std::endl;
				BudynekMieszkalny tmp = mieszkania_[i];
				tmp.print();
			}
		else
			std::cout << "<brak budynkow mieszkalnych w tej dzielnicy>" << std::endl;

		std::cout << std::endl << "Zaklady pracy:" << std::endl;
		if(zaklady_.size() > 0)
			for(unsigned int i = 0; i < zaklady_.size(); i++)
			{
				std::cout << i+1 << "." << std::endl;
				zaklady_[i].print();
			}
		else
			std::cout << "<brak zakladow pracy w tej dzielnicy>" << std::endl;
}

std::ostream & operator<< (std::ostream & os, const Dzielnica & d)
{
	os << std::endl << "----- Lista wszystkich obiektow w dzielnicy " << d.nazwa_ << ": -----" << std::endl;
	os << "Budynki mieszkalne:" << std::endl;
	if(d.mieszkania_.size() > 0)
		for(unsigned int i = 0; i < d.mieszkania_.size(); i++)
		{
			os << i+1 << "." << std::endl;
			os << d.mieszkania_[i];
		}
	else
		os << "<brak budynkow mieszkalnych w tej dzielnicy>" << std::endl;

	std::cout << std::endl << "Zaklady pracy:" << std::endl;
	if(d.zaklady_.size() > 0)
		for(unsigned int i = 0; i < d.zaklady_.size(); i++)
		{
			os << i+1 << "." << std::endl;
			os << d.zaklady_[i];
		}
	else
		os << "<brak zakladow pracy w tej dzielnicy>" << std::endl;

	return os;
}

bool Dzielnica::operator==(const Dzielnica & d)
{
	if(nazwa_ == d.nazwa_ && mieszkania_.get_allocator() == d.mieszkania_.get_allocator())
		return true;
	else
		return false;
}

std::string Dzielnica::getNazwa()
{
	return nazwa_;
}

void Dzielnica::save(ofstream& file)
{
	file << "<dzielnica name=\"" + nazwa_ + "\">";
	if (mieszkania_.size() > 0)
	{
		//file << "<budynkiMieszkalne>";
		for (unsigned int i = 0; i < mieszkania_.size(); i++)
			mieszkania_[i].save(file);
		//file << "</budynkiMieszkalne>";
	}
	if (zaklady_.size() > 0)
	{
		//file << "<zaklady>";
		for (unsigned int i = 0; i < zaklady_.size(); i++)
			zaklady_[i].save(file);
		//file << "</zaklady>";
	}
	file << "</dzielnica>";
}

void Dzielnica::load(string data, map<const string, Czlowiek*>& people)
{
	map<const string, string> districtAtts = XMLParser::attributes(data);

	if (!(districtAtts.find("name") == districtAtts.end()))
		nazwa_ = districtAtts.find("name")->second;

	vector<string> tmpBudynki = XMLParser::findIn("budynek", "dzielnica", data);
	for (int i = 0; i < tmpBudynki.size(); i++)
	{
		map<const string, string> atts = XMLParser::attributes(tmpBudynki[i]);
		vector<string> address = XMLParser::findIn("adres", "budynek", tmpBudynki[i]);
		map<const string, string> addr = XMLParser::attributes(address[0]);
		if (atts["type"] == "0") 
		{
			dodaj_budynek_mieszkalny(addr["ulica"], addr["kod"], addr["nrb"], stoi(addr["nrm"]));
		}
		else
		{
			dodaj_zaklad_pracy(addr["ulica"], addr["kod"], addr["nrb"], atts["name"], *people.find(atts["owner"])->second);
		}
	}
}
/*
template<typename T>
void Dzielnica::add(string street, string code, string buildingNo, unsigned int flatNo, string name, Czlowiek * owner)
{
	vector<T>* ptr;
	try
	{
		ptr = resolveBuildingType<T>();
	}
	catch (UnknownBuildingTypeException)
	{
		cerr << "Nie ma takiego typu budynku";
	}

	try
	{
		if (!checkIfExists<T>(ptr, street, code, buildingNo))
		{
			if (is_same<T, ZakladPracy>::value)
				ptr->emplace_back(street, code, buildingNo, name, owner);
			else if (is_same<T, BudynekMieszkalny>::value && flatNo == 0)
				ptr->emplace_back(street, code, buildingNo);
			else
				ptr->emplace_back(street, code, buildingNo, flatNo);
		}
	}
	catch (BuildingExistsException)
	{
		cerr << "Budynek o podanych danych juz istnieje";
	}
}

template<typename T>
bool Dzielnica::checkIfExists(vector<T>* ptr, string street, string code, string buildingNo)
{
	if (ptr->size() > 0)
	{
		for (unsigned int i = 0; i < ptr->size(); i++)
		{
			T obj = ptr->at(i);
			std::shared_ptr<Adres> tmp = obj.get_adres();
			if (tmp->get_ulica() == street && tmp->get_kod_pocztowy() == code && tmp->get_nr_budynku() == buildingNo)
				throw BuildingExistsException();
		}
	}

	return false;
}

template<typename T>
vector<T>* Dzielnica::resolveBuildingType()
{
	vector<T> *ptr;

	if (is_same<T, ZakladPracy>::value)
		ptr = zaklady_;
	else if (is_same<T, BudynekMieszkalny>::value)
		ptr = mieszkania_;
	else
		throw UnknownBuildingTypeException();

	return ptr;
}*/
