/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/
#pragma once
#ifndef WOJEWODZKIE_H
#define WOJEWODZKIE_H

#include "Miasto.h"

class MiastoWojewodzkie : public Miasto
{
public:
	void print_owner();
};

#endif