/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/
#pragma once
#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <iostream>
#include <regex>
#include <vector>
#include <map>
#include "Czlowiek.h"

using namespace std;

/**
 * Prosta klasa do analizy plik�w .xml
 */
class XMLParser
{
public:
	/**
	 * Rozbija ci�g znak�w na wektor, rozdziela wg podanego tagu
	 * @param string tag
	 * @param string source
	 * @return vector<string>
	 */
	static vector<string> split(string tag, string source);

	/**
	 * Analizuje pierwszy tag z podanego �r�d�a pod k�tem atrybut�w
	 * @param	string	source
	 * @return	map<const string, string>
	 */
	static map<const string, string> attributes(string source);

	static vector<string> findIn(string what, string from, string source);

	template<typename T>
	static map<const string, T*> createIdMap(T& type, string id, vector<string>);

	static string getMiasto(string source);
};

#endif