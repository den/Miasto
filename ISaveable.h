/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/
#pragma once

#ifndef ISAVEABLE_H
#define ISAVEABLE_H

#include <iostream>
#include <fstream>
#include <map>
//#include "Czlowiek.h"
class Czlowiek;

using namespace std;

class ISaveable
{
protected:
	ISaveable(){};
	virtual void save(ofstream&) = 0;
	virtual void load(string data, map<const string, Czlowiek*> &) = 0;
};

#endif