/**
 * Projekt nr 1 PROE, obiekt: Miasto
 * @author Marek Baranowski, 2T2
 */

#include "BudynekMieszkalny.h"

BudynekMieszkalny::BudynekMieszkalny()
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor mieszkania"<<std::endl;
	#endif

	wlasciciel_ = nullptr;
}

BudynekMieszkalny::BudynekMieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor mieszkania"<<std::endl;
	#endif

	wlasciciel_ = nullptr;
	adres_ = std::shared_ptr<Adres>(new Adres(ulica, kod_pocztowy, nr_budynku));
}

BudynekMieszkalny::BudynekMieszkalny(std::string ulica, std::string kod_pocztowy, std::string nr_budynku, int nr_mieszkania)
{
	#ifdef TESTPR1
		std::cout<<"INFO: Konstruktor mieszkania"<<std::endl;
	#endif

	wlasciciel_ = nullptr;
	adres_ = std::shared_ptr<Adres>(new Adres(ulica, kod_pocztowy, nr_budynku, nr_mieszkania));
}

void BudynekMieszkalny::load(string data, map<const string, Czlowiek*> & people)
{

}

void BudynekMieszkalny::save(ofstream& file)
{
	file << "<budynek type=\"0\" name=\"" + nazwa_ + "\" owner=\"" << wlasciciel_ << "\">";
	adres_->save(file);
	//file << "<wlasciciel ptr=\"" << wlasciciel_ << "\"></wlasciciel>";
	file << "</budynek>";
}