/**
* Projekt nr 2 PROE, obiekt: Miasto
* @author Marek Baranowski, 2T2
*/

#include <iostream>
#include <map>
#include "XMLParser.h"

using namespace std;

vector<string> XMLParser::split(string tag, string source)
{
	vector<string> result;

	regex pattern("(<" + tag + "(?:[^>])*?>(.*?)</" + tag + ">)+?");
	smatch matches;

	while (regex_search(source, matches, pattern))
	{
		result.push_back(matches[1]);
		source = regex_replace(source, pattern, "", regex_constants::format_first_only);
	}

	return result;
}

map<const string, string> XMLParser::attributes(string source)
{
	map<const string, string> attributes;
	smatch result;
	regex pattern("^<[a-zA-Z]+ (([a-z]+?)=\"(.+?))+?\"[ ]*?>");

	while (regex_search(source, result, pattern))
	{
		attributes.emplace(result[2], result[3]);
		regex tmp(" " + result[2].str() + "=\"" + result[3].str() + "\"");
		source = regex_replace(source, tmp, "");
	}

	return attributes;
}

vector<string> XMLParser::findIn(string what, string from, string source)
{
	vector<string> result;

	regex pattern("<" + from + "(?:[^>])*?>(?:.)*?(<" + what + "([^<>\/]*?)>(?:.)*?</" + what + ">)(?:.)*?</" + from + ">");
	regex pattern2("<" + from + "(?:[^>])*?>(<" + what + "([^<>\/]*?)>(?:.)*?</" + what + ">)");
	smatch matches;

	while (regex_search(source, matches, pattern))
	{
		result.push_back(matches[1]);
		source = regex_replace(source, pattern2, "<" + from + ">", regex_constants::format_first_only);
	}

	return result;
}

template<typename T>
map<const string, T*> XMLParser::createIdMap(T& type, string id, vector<string> source)
{

}

string XMLParser::getMiasto(string source)
{
	vector<string> result;
	regex pattern("<ludzie>(?:.)*?</ludzie>");
	smatch matches;
	source = regex_replace(source, pattern, "");

	return source;
}
